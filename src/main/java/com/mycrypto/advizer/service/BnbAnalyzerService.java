package com.mycrypto.advizer.service;

import com.mycrypto.advizer.configuration.CryptoProperties;
import com.mycrypto.advizer.dao.CoinPriceDataLoader;
import com.mycrypto.advizer.domain.Coin;
import com.mycrypto.advizer.domain.KlineEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Service(Coin.TypeCoins.BNBUSDT)
public class BnbAnalyzerService extends AbstractAnalyzerService {

    @Autowired
    @Qualifier("bnbDataLoader")
    private CoinPriceDataLoader dataLoader;
    @Autowired
    private MessageSender sender;
    @Autowired
    @Qualifier("bnbProperties")
    private CryptoProperties properties;

    @Override
    public void checkCoinPrice() {
        String coin = Coin.TypeCoins.BNBUSDT;
        LinkedList<KlineEntity> data = dataLoader.loadData(coin);
        String message = analyzeData(Coin.BNB.getValue(), data, properties);
        if (isNotEmpty(message)) {
            sender.sendAsync(message);
        }
    }
}
