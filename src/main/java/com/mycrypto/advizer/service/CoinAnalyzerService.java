package com.mycrypto.advizer.service;

public interface CoinAnalyzerService {

    void checkCoinPrice();
}
