package com.mycrypto.advizer.service;

import com.mycrypto.advizer.configuration.CryptoProperties;
import com.mycrypto.advizer.dao.CoinPriceDataLoader;
import com.mycrypto.advizer.domain.Coin;
import com.mycrypto.advizer.domain.KlineEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Service(Coin.TypeCoins.POLYXUSDT)
public class PolyxAnalyzerService extends AbstractAnalyzerService {

    @Autowired
    @Qualifier("polyxDataLoader")
    private CoinPriceDataLoader dataLoader;
    @Autowired
    private MessageSender sender;
    @Autowired
    @Qualifier("polyxProperties")
    private CryptoProperties properties;

    @Override
    public void checkCoinPrice() {
        String coin = Coin.TypeCoins.POLYXUSDT;
        LinkedList<KlineEntity> data = dataLoader.loadData(coin);
        String message = analyzeData(Coin.ETH.getValue(), data, properties);
        if (isNotEmpty(message)) {
            sender.sendAsync(message);
        }
    }
}
