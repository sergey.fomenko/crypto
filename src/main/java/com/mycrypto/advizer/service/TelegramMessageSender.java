package com.mycrypto.advizer.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.CompletableFuture;

import static org.apache.commons.lang3.StringUtils.EMPTY;

@Slf4j
@Service
public class TelegramMessageSender implements MessageSender {

    private String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
    private String apiToken = "7058939612:AAEBmFle9xhpW-zsTKmdPDGmJMhF4mPeD40";
    private String chatId = "-1002013248004";

    @Override
    @Async
    public CompletableFuture<Void> sendAsync(String text) {
        String uri = String.format(urlString, apiToken, chatId, text);
        try {
            URL url = new URL(uri);
            URLConnection conn = url.openConnection();
            InputStream is = new BufferedInputStream(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = EMPTY;
            StringBuilder sb = new StringBuilder();
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            String response = sb.toString();
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
        return null;
    }
}