package com.mycrypto.advizer.service;

import com.mycrypto.advizer.domain.KlineEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.DoubleStream;

import static com.mycrypto.advizer.util.Utility.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import com.mycrypto.advizer.configuration.CryptoProperties;

@Slf4j
public abstract class AbstractAnalyzerService implements CoinAnalyzerService {

    protected static final double MIN_CRYPTO_AMOUNT = 0.00001d;
    protected static final double MIN_EQUITY_AMOUNT = 1d;
    protected static final String BUY = " buy ";
    protected static final String SELL = " sell ";
    protected static final int AFTER = 7;
    protected double ZERO = 0.0d;
    protected int dealCount = 0;
    protected String message = EMPTY;
    protected Double maxMarker = Double.MAX_VALUE;
    protected Double minMarker = Double.MIN_VALUE;
    protected Double lastMessagePrice = 0.0;

    protected String analyzeData(String coin,
                                 LinkedList<KlineEntity> data,
                                 CryptoProperties properties) {
        Double equity = properties.getStartEquity();
        Double cryptoAmount = properties.getStartCrypto();
        init(data, properties.getDropMoreThan(), coin);
        double lastPrice = 0.0;
        Iterator<KlineEntity> iterator = data.iterator();
        while (iterator.hasNext()) {
            KlineEntity entity = iterator.next();
            Double currentPrice = entity.getClose();
            if (currentPrice < minMarker) {
                if (isAmountAvailable(equity, MIN_EQUITY_AMOUNT)) {
                    Pair<Double, Double> pair = buyCrypto(entity, equity, cryptoAmount);
                    equity = pair.getLeft();
                    cryptoAmount = pair.getRight();
                }
                if (entity.getOpenTime().isAfter(LocalDateTime.now().minusHours(AFTER))) {
                    lastPrice = currentPrice;
                    message = getTimeToMessage(entity, BUY, coin, currentPrice, minMarker);
                }
            }
            if (currentPrice > maxMarker) {
                if (isAmountAvailable(cryptoAmount, MIN_CRYPTO_AMOUNT)) {
                    Pair<Double, Double> pair = sellCrypto(entity, equity, cryptoAmount);
                    equity = pair.getLeft();
                    cryptoAmount = pair.getRight();
                }
                if (entity.getOpenTime().isAfter(LocalDateTime.now().minusHours(AFTER))) {
                    message = getTimeToMessage(entity, SELL, coin, currentPrice, maxMarker);
                    lastPrice = currentPrice;
                }
            }
        }
        log.info(message);
        if (lastMessagePrice.equals(lastPrice)) {
            message = EMPTY;
        } else {
            lastMessagePrice = lastPrice;
        }
        return message;
    }

    private void init(LinkedList<KlineEntity> data,
                      double minDiffPercent,
                      String coin) {
        double maxDouble = getDoubleStream(data).max().orElse(0d);
        double minDouble = getDoubleStream(data).min().orElse(0d);
        double diff = getDiffPercentBetween(maxDouble, minDouble);
        if (diff > minDiffPercent) {
            minMarker = minDouble * 1.02;
            maxMarker = maxDouble * 0.98;
            print(data, coin, minDouble, maxDouble, minMarker, maxMarker);
        }
    }

    private static DoubleStream getDoubleStream(LinkedList<KlineEntity> data) {
        return data.stream()
                .mapToDouble(KlineEntity::getHigh)
                .filter(e -> e > 0.01);
    }

    private Pair<Double, Double> buyCrypto(KlineEntity klineEntity,
                                           double equity,
                                           double cryptoAmount) {
        double buyAmount = equity / klineEntity.getClose();
        double newCryptoAmount = cryptoAmount + buyAmount;
        equity = equity - buyAmount * klineEntity.getClose();
        dealCount++;
        return new ImmutablePair<>(equity, newCryptoAmount);
    }

    private Pair<Double, Double> sellCrypto(KlineEntity klineEntity,
                                            double equity,
                                            double cryptoAmount) {
        double buyAmount = cryptoAmount * klineEntity.getClose();
        double newCryptoAmount = ZERO;
        equity = equity + buyAmount;
        dealCount++;
        return new ImmutablePair<>(equity, newCryptoAmount);
    }
}
