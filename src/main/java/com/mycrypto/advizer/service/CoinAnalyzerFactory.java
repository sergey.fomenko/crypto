package com.mycrypto.advizer.service;

import com.mycrypto.advizer.domain.Coin;

public interface CoinAnalyzerFactory {

    CoinAnalyzerService getService(Coin coin);
}
