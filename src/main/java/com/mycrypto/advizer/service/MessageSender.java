package com.mycrypto.advizer.service;

import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

public interface MessageSender {

    @Async
    CompletableFuture<Void> sendAsync(String text);
}
