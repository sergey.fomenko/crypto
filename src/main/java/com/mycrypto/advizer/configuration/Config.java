package com.mycrypto.advizer.configuration;

import com.mycrypto.advizer.service.CoinAnalyzerFactory;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableAsync
@EnableScheduling
public class Config {

    @Bean
    @ConfigurationProperties("api.binance")
    public BinanceProperties binanceProperties() {
        return new BinanceProperties();
    }

    @Bean
    @ConfigurationProperties("crypto.ethereum")
    public CryptoProperties ethereumProperties() {
        return new EthereumProperties();
    }

    @Bean
    @ConfigurationProperties("crypto.default")
    public CryptoProperties cryptoProperties() {
        return new CryptoProperties();
    }

    @Bean
    @ConfigurationProperties("crypto.sol")
    public CryptoProperties solProperties() {
        return new SolProperties();
    }

    @Bean
    @ConfigurationProperties("crypto.bnb")
    public CryptoProperties bnbProperties() {
        return new BnbProperties();
    }

    @Bean
    @ConfigurationProperties("crypto.polyx")
    public CryptoProperties polyxProperties() {
        return new PolyxProperties();
    }

    @Bean
    public ServiceLocatorFactoryBean serviceLocatorFactoryBean() {
        ServiceLocatorFactoryBean slfb = new ServiceLocatorFactoryBean();
        slfb.setServiceLocatorInterface(CoinAnalyzerFactory.class);
        return slfb;
    }
}
