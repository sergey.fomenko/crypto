package com.mycrypto.advizer.configuration;

import lombok.Data;

@Data
public class BinanceProperties {
    private String interval;
    private String url;
}

