package com.mycrypto.advizer.configuration;

import lombok.Data;

@Data
public class CryptoProperties {
    private double minMarginToBuy;
    private double maxMarginToBuy;
    private double minMarginToSell;
    private int minCounterToBuy;
    private int minCounterToSell;
    private double startEquity;
    private double startCrypto;
    private double dropMoreThan;
}
