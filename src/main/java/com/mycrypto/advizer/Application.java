package com.mycrypto.advizer;

import com.mycrypto.advizer.domain.Coin;
import com.mycrypto.advizer.service.CoinAnalyzerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private CoinAnalyzerFactory factory;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        run();
    }

    @Scheduled(cron = "${run.scheduler}", zone = "Europe/Paris")
    public void run() {
        List<Coin> coins = List.of(Coin.BTC, Coin.ETH, Coin.BNB, Coin.SOL);
        for (Coin coin : coins) {
            factory.getService(coin).checkCoinPrice();
        }
    }
}
