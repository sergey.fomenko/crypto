package com.mycrypto.advizer.util;

import com.mycrypto.advizer.configuration.CryptoProperties;
import com.mycrypto.advizer.domain.KlineEntity;
import com.mycrypto.advizer.domain.TradingVector;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import static com.mycrypto.advizer.domain.TradingVector.DOWN;
import static com.mycrypto.advizer.domain.TradingVector.UP;

@Slf4j
@UtilityClass
public class Utility {

    public static double getDiffPercentBetween(Double max,
                                               Double min) {
        return ((max - min) / ((max + min) / 2)) * 100;
    }

    public static boolean isAmountAvailable(double amount,
                                            double minAmount) {
        return amount > minAmount;
    }

    public static String getTimeToMessage(KlineEntity entity,
                                          String buy,
                                          String coin,
                                          double currentPrice,
                                          double marker) {
        return "Time (" + entity.getOpenTime().toLocalTime() + ") to" + buy + coin +
                ", by " + String.format("%.4f", currentPrice) +
                ", marker: " + String.format("%.4f", marker);
    }

    public static void print(LinkedList<KlineEntity> data,
                             String coin,
                             double minDouble,
                             double maxDouble,
                             double minMarker,
                             double maxMarker) {
        log.info(" ");
        log.info("---- Markers for the last 3 days for " + coin + " ----");
        log.info("Min: " + String.format("%.4f", minDouble) + ", marker: " + String.format("%.4f", minMarker));
        log.info("Max: " + String.format("%.4f", maxDouble) + ", marker: " + String.format("%.4f", maxMarker));
        log.info("Current price: " + String.format("%.4f", data.getLast().getClose()));
    }
}
