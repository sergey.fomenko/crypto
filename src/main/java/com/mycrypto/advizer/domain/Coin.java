package com.mycrypto.advizer.domain;

import lombok.Getter;

public enum Coin {

    BTC(TypeCoins.BTCUSDT, "BITCOIN"),
    SOL(TypeCoins.SOLUSDT, "SOLANA"),
    BNB(TypeCoins.BNBUSDT, "BNB"),
    ETH(TypeCoins.ETHUSDT, "ETHEREUM"),
    POLYX(TypeCoins.POLYXUSDT, "POLYX");

    private final String name;
    @Getter
    private final String value;

    Coin(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public interface TypeCoins {
        String BTCUSDT = "BTCUSDT";
        String SOLUSDT = "SOLUSDT";
        String BNBUSDT = "BNBUSDT";
        String ETHUSDT = "ETHUSDT";
        String POLYXUSDT = "POLYXUSDT";
    }
}


