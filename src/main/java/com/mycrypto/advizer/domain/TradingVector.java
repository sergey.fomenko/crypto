package com.mycrypto.advizer.domain;

public enum TradingVector {
    UP, DOWN
}
