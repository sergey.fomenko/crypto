package com.mycrypto.advizer.domain;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KlineEntity {

    @CsvDate(value = "yyyy-MM-dd'T'HH:mm")
    @CsvBindByName
    private LocalDateTime openTime;

    @CsvBindByName
    private double open;

    @CsvBindByName
    private double close;

    @CsvBindByName
    private String symbol;

    @CsvBindByName
    private double high;

    @CsvBindByName
    private double low;

    @Override
    public String toString(){
        StringBuilder dataBuilder = new StringBuilder();
        appendFieldValue(dataBuilder, openTime.toString());
        appendFieldValue(dataBuilder, String.valueOf(open));
        appendFieldValue(dataBuilder, String.valueOf(close));
        appendFieldValue(dataBuilder, String.valueOf(high));
        appendFieldValue(dataBuilder, String.valueOf(low));
        appendFieldValue(dataBuilder, symbol);

        return dataBuilder.toString();
    }

    private void appendFieldValue(StringBuilder dataBuilder,
                                  String fieldValue) {
        if(fieldValue != null) {
            dataBuilder.append(fieldValue.replace(".", ",")).append(";");
        } else {
            dataBuilder.append("").append("\n");
        }
    }
}
