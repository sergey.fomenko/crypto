package com.mycrypto.advizer.dao;

import com.mycrypto.advizer.configuration.BinanceProperties;
import com.mycrypto.advizer.domain.KlineEntity;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Slf4j
public abstract class BinanceDataLoader implements CoinPriceDataLoader {

    @Autowired
    protected BinanceProperties properties;
    protected String SYMBOL;
    protected LocalDateTime to;
    protected LocalDateTime from;

    @Override
    public LinkedList<KlineEntity> loadData(String coin) {
        SYMBOL = coin;
        HttpClient httpClient = HttpClient.newHttpClient();
        to = LocalDateTime.now();
        int i = 4 * 24 * 3; // 60m/15m; 24h; 3d
        from = to.minusHours(i);
        Path path = Paths.get("src", "main", "resources", "stat_" + SYMBOL + "_" + properties.getInterval() + ".csv");
        LinkedList<KlineEntity> data = loadData(from, to, httpClient, path);
        LinkedList<KlineEntity> last = getLastData(LocalDateTime.now().minusMinutes(1), LocalDateTime.now(), httpClient);
        data.addAll(last);
        return data;
    }

    protected LinkedList<KlineEntity> loadData(LocalDateTime from,
                                               LocalDateTime to,
                                               HttpClient httpClient,
                                               Path path) {
        LinkedList<KlineEntity> statEntities = new LinkedList<>();
        try {
            statEntities = readEntityFromCsv(path, KlineEntity.class);
            if (!statEntities.isEmpty()) {
                from = statEntities.getLast().getOpenTime();
                getData(from, to, httpClient, 1, statEntities, properties.getInterval());
            }
            if (statEntities.isEmpty()) {
                getData(from, to, httpClient, 0, statEntities, properties.getInterval());
            }
            if (!statEntities.isEmpty()) {
                writeCsvFromData(path, statEntities);
                statEntities = readEntityFromCsv(path, KlineEntity.class);
                return statEntities;
            }
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
        return statEntities;
    }

    private void getData(LocalDateTime from,
                         LocalDateTime to,
                         HttpClient httpClient,
                         int firstLine, LinkedList<KlineEntity> statEntities, String interval)
            throws URISyntaxException, IOException, InterruptedException {
        for (LocalDateTime startDate = from; startDate.isBefore(to); startDate = startDate.plusDays(1)) {

            long startTime = Timestamp.valueOf(startDate).getTime();
            long endTime = Timestamp.valueOf(to).getTime();
            String url = String.format(properties.getUrl(), SYMBOL, interval, startTime, endTime);
            HttpRequest request = HttpRequest.newBuilder().uri(new URI(url)).GET().build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            JSONArray klines = new JSONArray(response.body());

            for (int i = firstLine; i < klines.length(); i++) {
                JSONArray kline = klines.getJSONArray(i);
                KlineEntity klineEntity = createKlineEntity(kline, SYMBOL);
                statEntities.add(klineEntity);
            }
        }
    }

    private LinkedList<KlineEntity> getLastData(LocalDateTime startDate, LocalDateTime to, HttpClient httpClient) {
        long startTime = Timestamp.valueOf(startDate).getTime();
        long endTime = Timestamp.valueOf(to).getTime();
        String url = String.format(properties.getUrl(), SYMBOL, "1m", startTime, endTime);
        HttpRequest request;
        LinkedList<KlineEntity> statEntities = new LinkedList<KlineEntity>();
        try {
            request = HttpRequest.newBuilder().uri(new URI(url)).GET().build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            JSONArray klines = new JSONArray(response.body());
            JSONArray kline = klines.getJSONArray(0);
            KlineEntity klineEntity = createKlineEntity(kline, SYMBOL);
            statEntities.add(klineEntity);
            return statEntities;
        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    protected KlineEntity createKlineEntity(JSONArray kline,
                                            String symbol) {
        KlineEntity klineEntity = KlineEntity.builder()
                .openTime(LocalDateTime.ofInstant(Instant
                        .ofEpochMilli(kline.getLong(0)), TimeZone.getDefault().toZoneId()))
                .open(kline.getDouble(1))
                .high(kline.getDouble(2))
                .low(kline.getDouble(3))
                .close(kline.getDouble(4))
                .symbol(symbol)
                .build();
        return klineEntity;
    }

    protected LinkedList readEntityFromCsv(Path path, Class clazz) {
        try (Reader reader = Files.newBufferedReader(path)) {
            CsvToBean cb = new CsvToBeanBuilder<KlineEntity>(reader)
                    .withType(clazz)
                    .withSeparator(';')
                    .build();
            List<KlineEntity> parse = cb.parse();
            LinkedList<KlineEntity> collect = (LinkedList<KlineEntity>) parse.stream()
                    .sorted(Comparator.comparing(KlineEntity::getOpenTime))
                    .filter(e -> e.getOpenTime().isAfter(to.minusDays(3)))
                    .distinct()
                    .collect(Collectors.toCollection(LinkedList::new));
            return collect;
        } catch (IOException e) {
            return new LinkedList<>();
        }
    }

    protected void writeCsvFromData(Path path,
                                    List<KlineEntity> data) throws Exception {
        try (Writer writer  = new FileWriter(path.toString())) {
            StatefulBeanToCsv<KlineEntity> sbc = new StatefulBeanToCsvBuilder<KlineEntity>(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withSeparator(';')
                    .build();
            sbc.write(data);
        }
    }
}
