package com.mycrypto.advizer.dao;

import com.mycrypto.advizer.domain.KlineEntity;

import java.util.LinkedList;

public interface CoinPriceDataLoader {
    LinkedList<KlineEntity> loadData(String coin);
}
